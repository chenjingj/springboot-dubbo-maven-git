package com.jk.mapper;

import com.jk.model.Car;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface CarMapper {
    void saveCar(Car car);

    List<Car> selectCar();

    void deleteCar(@Param("carId") Integer carId);

    Car queryCarById(@Param("carId") Integer carId);

    void editCar(Car car);
}
