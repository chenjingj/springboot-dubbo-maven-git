package com.jk.service;

import com.alibaba.dubbo.config.annotation.Service;
import com.jk.mapper.CarMapper;
import com.jk.model.Car;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Date;
import java.util.List;

@Service(interfaceClass = CarService.class)
@Component
public class CarServiceImpl implements CarService {

    @Autowired
    private CarMapper carMapper;
    @Override
    public void saveCar(Car car) {
        car.setCarTime(new Date());
        carMapper.saveCar(car);
    }

    @Override
    public List<Car> selectCar() {
        return carMapper.selectCar();
    }

    @Override
    public void deleteCar(Integer carId) {
        carMapper.deleteCar(carId);
    }

    @Override
    public Car queryCarById(Integer carId) {
        return carMapper.queryCarById(carId);
    }

    @Override
    public void editCar(Car car) {
        carMapper.editCar(car);
    }
}
