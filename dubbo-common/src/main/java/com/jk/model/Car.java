package com.jk.model;

import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;
import java.util.Date;

@Data
public class Car implements Serializable {

    private Integer carId;

    private String carBrand;

    private Integer carPrice;

    private String carColor;
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date carTime;
}
