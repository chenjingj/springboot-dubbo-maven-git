package com.jk.model;

import lombok.Data;

import java.io.Serializable;
import java.util.List;
@Data
public class Tree implements Serializable {

    private Integer id;

    private String text;

    private Integer pid;

    private Boolean selectable;

    private List<Tree> nodes;

    private String href;
}
