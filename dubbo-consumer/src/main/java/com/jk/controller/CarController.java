package com.jk.controller;

import com.alibaba.dubbo.config.annotation.Reference;
import com.jk.model.Car;
import com.jk.service.CarService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;

@Controller
public class CarController {

    @Reference
    private CarService carService;

    @RequestMapping("toAdd")
    public String toAdd(){
        return "addCar";
    }

    @RequestMapping("saveCar")
    public String  saveCar(Car car){
        carService.saveCar(car);
        return "redirect:selectCar";
    }

    @RequestMapping("selectCar")
    public String selectCar(Model model){
       List<Car> car = carService.selectCar();
        model.addAttribute("cars",car);
        return "carList";
    }

    /*
   删除
    */
    @RequestMapping("deleteCar")
    public String deleteCar(Integer carId){
        carService.deleteCar(carId);
        return "redirect:selectCar";
    }

    /*
    修改  查询
     */
    @RequestMapping("queryCarById")
    public String queryCarById(Integer carId,Model model){
        Car car = carService.queryCarById(carId);
        model.addAttribute("car",car);
        return "editCar";
    }

    /*
    修改方法
     */
    @RequestMapping("editCar")
    public String editCar(Car car) {
        carService.editCar(car);
        return "redirect:selectCar";
    }

    @RequestMapping("tosave")
    public String tosave(){
        return "tosave";
    }

}
