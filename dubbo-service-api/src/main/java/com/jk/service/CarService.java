package com.jk.service;

import com.jk.model.Car;

import java.util.List;

public interface CarService {

    void saveCar(Car car);
    List<Car> selectCar();
    void deleteCar(Integer carId);
    Car queryCarById(Integer carId);
    void editCar(Car car);
}
